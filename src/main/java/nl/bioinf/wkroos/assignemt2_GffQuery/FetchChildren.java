/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.wkroos.assignemt2_GffQuery;

import java.util.ArrayList;

/**
 *
 * @author Willem
 */
public class FetchChildren {
//  this function finds the childeren of the given parentID.
//    when a child is found it will call this function with its ID.

    static ArrayList<Integer> findChildren(String parentID, ArrayList<LineFeatures> lineFeaturesList, ArrayList<Integer> saveLines) {
        for (int i = 0; i < lineFeaturesList.size(); i++) {
//            if (i > 1476 && i < 1525) {

            LineFeatures lineFeatures = lineFeaturesList.get(i);
            if (lineFeatures.getLineCommand().containsKey("Parent")) {
                String parentString = (String) lineFeatures.getLineCommand().get("Parent");

                for (String parentLine : parentString.split(",")) {
                    if (parentLine.toLowerCase().equals(parentID.toLowerCase())) {
                        if (lineFeatures.getLineCommand().containsKey("ID")) {
                            System.out.println("parentString = " + parentString);
//                            saveLines.add(i);
                            findChildren((String) lineFeatures.getLineCommand().get("ID"), lineFeaturesList, saveLines);
                        }

                        saveLines.add(i);
                    }

                }
            }
//            }
        }
        return saveLines;
    }

}
