/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.wkroos.assignemt2_GffQuery;

import java.util.HashMap;

/**
 *
 * @author Willem
 */
public class LineFeatures {

    private int lineNumber;
    private String seqname;
    private String source;
    private String feature;
    private int startPosition;
    private int eindPosition;
    private String score;
    private char strand;
    private String frame;
    private final HashMap lineCommand = new HashMap<>();

    /**
     *
     * @return
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     *
     * @param lineNumber
     */
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    /**
     *
     * @return
     */
    public String getSeqname() {
        return seqname;
    }

    /**
     *
     * @param seqname
     */
    public void setSeqname(String seqname) {
        this.seqname = seqname;
    }

    /**
     *
     * @return
     */
    public String getSource() {
        return source;
    }

    /**
     *
     * @param source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     *
     * @return
     */
    public String getFeature() {
        return feature;
    }

    /**
     *
     * @param feature
     */
    public void setFeature(String feature) {
        this.feature = feature;
    }

    /**
     *
     * @return
     */
    public int getStartPosition() {
        return startPosition;
    }

    /**
     *
     * @param startPosition
     */
    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    /**
     *
     * @return
     */
    public int getEindPosition() {
        return eindPosition;
    }

    /**
     *
     * @param eindPosition
     */
    public void setEindPosition(int eindPosition) {
        this.eindPosition = eindPosition;
    }

    /**
     *
     * @return
     */
    public String getScore() {
        return score;
    }

    /**
     *
     * @param score
     */
    public void setScore(String score) {
        this.score = score;
    }

    /**
     *
     * @return
     */
    public char getStrand() {
        return strand;
    }

    /**
     *
     * @param strand
     */
    public void setStrand(char strand) {
        this.strand = strand;
    }

    /**
     *
     * @return
     */
    public String getFrame() {
        return frame;
    }

    /**
     *
     * @param frame
     */
    public void setFrame(String frame) {
        this.frame = frame;
    }

    /**
     *
     * @return
     */
    public HashMap getLineCommand() {
        return lineCommand;
    }

    /**
     *
     * @param lineCommand puts the values of field nine in a dictionary
     */
    public void setLineCommand(String lineCommand) {

        for (String splittedLineCommand1 : lineCommand.split(";")) {
            if (!splittedLineCommand1.trim().isEmpty()) {

                this.lineCommand.put(splittedLineCommand1.split("=")[0].trim(), splittedLineCommand1.split("=")[1].trim());
            }
        }

    }

    @Override
    public String toString() {
        return "LineFeatures{" + "lineNumber=" + lineNumber + ", seqname=" + seqname + ", source=" + source + ", feature=" + feature + ", startPosition=" + startPosition + ", eindPosition=" + eindPosition + ", score=" + score + ", strand=" + strand + ", frame=" + frame + ", lineCommand=" + lineCommand.toString() + '}';
    }

}
