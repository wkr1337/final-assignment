/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.wkroos.assignemt2_GffQuery;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 * @author Willem
 */
public class FindWildcard {
//      this function finds the given wild card in field nine of the given file
//    it usses a regular expression.

    static boolean findWildCard(String pattern, LineFeatures lineFeatures) {

        Iterator it = lineFeatures.getLineCommand().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (Pattern.compile(pattern).matcher((String) pair.getValue()).find()) {
                return true;
            }
        }
        return false;
    }

}
