/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package nl.bioinf.wkroos.assignemt2_GffQuery;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Willem Roos
 * @version 0.0.1
 */
public final class GffQuery {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws org.apache.commons.cli.ParseException
     */
    public static void main(final String[] args) throws IOException, ParseException {
        GffQuery mainObject = new GffQuery();

        HelpFormatter formatter = new HelpFormatter();
//        add command line input options to help formatter
        Options options = new Options();
        options.addOption("h", "help", false, "displays help");
        options.addOption("i", "infile", true, "path to the .gff3 file");
        options.addOption("s", "summary", false, "Creates a textual summary of the parsed file: names of molecules with annotations, types and counts of features.");
        options.addOption("ft", "fetch_type", true, "Will fetch a certain type of feature (e.g. gene, CDS) \nReturns a GFF3-type listing of all features of this type");
        options.addOption("f", "filter", true, "The filter should be specified using the format \"source|score|orientation|maximum_length|minimum_length\", \n"
                + "    where suppression of an individual filter is indicated using an asterisk (*).");
        options.addOption("fr", "fetch_region", true, "Will select all features that are included completely within the given coordinates\n"
                + "the format sould be like: \"250000..260000\"");
        options.addOption("fw", "find_wildcard", true, "Lists all features that have the given wildrcard string (a regex string) in the Name attribute (part of field 9)");
        options.addOption("fc", "fetch_children", true, "Returns a GFF3-type listing RECURSIVELY as GFF3-type listing that have the given Parent ID as ancestor (parent or parents parent!)");
//        try to parse the command line arguments and read the file and run the
//        program depending on what command line arguments are given.

        try {
            BasicParser bp = new BasicParser();
            CommandLine cl = bp.parse(options, args);
            if (cl.hasOption("h") || args.length == 0) {
                System.out.println("args.length = " + Arrays.toString(args));
                formatter.printHelp("Gff3 File Parser", options);
            } else {
                if (cl.hasOption("infile")) {

                    if (args.length == 2) {
                        System.out.print("select filter ");
                        formatter.printHelp("Gff3 File Parser", options);
                    } else {
                        mainObject.start(cl);
                    }

                } else {
//                    when no file is given show it to the user
                    System.out.print("You have not provided a file to parse, use --infile \"path/to/file.gff3\"");
                    formatter.printHelp("Gff3 File Parser", options);

                }
            }

        } catch (ParseException ex) {
            System.out.print("You provided an ill-formatted Command Line: " + ex.getMessage());
            System.out.println(Arrays.toString(args));
            formatter.printHelp("Gff3 File Parser", options);

        }

    }

    /**
     * starts the application.
     *
     * @throws java.io.IOException
     */
//    this function starts the whole program, it checks if summary is given than
//    it will print the summary else it will proceed to startFilters to start filtering
    private void start(CommandLine cl) throws IOException {
        ArrayList<LineFeatures> myList = this.readFile(cl.getOptionValue("infile"));
        ArrayList<Integer> saveLines = new ArrayList();
        ArrayList<Integer> intList = new ArrayList();
        FilterFunctions ff = new FilterFunctions();
        ArrayList<LineFeatures> filteredList = new ArrayList<>();
        Summary summaryInstance = new Summary();
        summaryInstance.setFileName(cl.getOptionValue("infile"));

//        if (cl.hasOption("fetch_children")) {
//
//            intList = FetchChildren.findChildren(cl.getOptionValue("fetch_children"), myList, saveLines);
//        }
        filteredList = startFilters(cl, myList, summaryInstance, ff, filteredList);
        if (cl.hasOption("summary")) {
            System.out.println("summary = " + summaryInstance.toString());
        } else {
            for (LineFeatures filteredList1 : filteredList) {
                System.out.println("filteredList1 = " + filteredList1);
            }
        }
    }
//
//      this function starts the filters it runs all the filters if any are given
//    and prints the output to the user

    private ArrayList<LineFeatures> startFilters(CommandLine cl, ArrayList<LineFeatures> myList, Summary summaryInstance, FilterFunctions ff, ArrayList<LineFeatures> filteredList) {
        ArrayList<Integer> saveLines = new ArrayList();
        ArrayList<Integer> intList = new ArrayList();
        if (cl.hasOption("fetch_children")) {

            intList = FetchChildren.findChildren(cl.getOptionValue("fetch_children"), myList, saveLines);
        }

        for (int i = 0; i < myList.size(); i++) {
            boolean linePassesTests = true;
            if (!intList.isEmpty()) {
                if (!intList.contains(i)) {
                    linePassesTests = false;
                }
            }

            LineFeatures obj = myList.get(i);

            if (cl.hasOption("summary")) {
                summaryInstance.countFeature(obj);
                summaryInstance.countSeqName(obj);
                linePassesTests = false;
            }
            if (cl.hasOption("fetch_type") && linePassesTests) {
                String fetch_type = cl.getOptionValue("fetch_type");
                if (!ff.getFeature(fetch_type, obj)) {
                    linePassesTests = false;
                }

            }
            if (cl.hasOption("filter") && linePassesTests) {
                String inputFilter = cl.getOptionValue("filter");
                String[] splittedString = inputFilter.split("\\|");
                if (!ff.filterFilterInput(splittedString, obj)) {
                    linePassesTests = false;
                }

            }
//            get lines from between the given region; given by --fetch-region input
            if (cl.hasOption("fetch_region") && linePassesTests) {
                String fetch_region = cl.getOptionValue("fetch_region");
                if (!ff.filterRegion(fetch_region, obj)) {
                    linePassesTests = false;
                }

            }
            if (cl.hasOption("find_wildcard") && linePassesTests) {
                String wildCard = cl.getOptionValue("find_wildcard");
                if (!FindWildcard.findWildCard(wildCard, obj)) {

                    linePassesTests = false;
                }

            }

//            linePassesTests
            if (linePassesTests) {
                filteredList.add(obj);
            }
        }
        return filteredList;
    }
//  this function reads the file line by line and makes a LineFeature object
//    of every line

    private ArrayList readFile(String givenFile) throws FileNotFoundException, IOException {
        ArrayList<LineFeatures> myList = new ArrayList<>();

        BufferedReader bufferdReaderInstance = new BufferedReader(new FileReader(givenFile));
        StringBuilder stringBuilderInstance = new StringBuilder();
        String line = bufferdReaderInstance.readLine();
        int counter = 0;
        while (line != null) {
            counter++;
            stringBuilderInstance.append(line);

            stringBuilderInstance.append(System.lineSeparator());

            String[] splitted = line.split("\\s+");
            if (splitted.length < 8) {

            } else {
                myList.add(this.makeLineFeature(splitted, counter));
            }

            line = bufferdReaderInstance.readLine();

        }
        return myList;
    }
//this function sets the values for a LineFeature object

    private LineFeatures makeLineFeature(String[] splittedLine, int counter) {
        LineFeatures lf = new LineFeatures();
        lf.setLineNumber(counter);
        lf.setSeqname(splittedLine[0]);
        lf.setSource(splittedLine[1]);
        lf.setFeature(splittedLine[2]);
        lf.setStartPosition((Integer.parseInt(splittedLine[3])));
        lf.setEindPosition(Integer.parseInt(splittedLine[4]));
        lf.setScore(splittedLine[5]);
        lf.setStrand(splittedLine[6].charAt(0));
        lf.setFrame(splittedLine[7]);
        String fieldNineString = "";
//      when the line is splitted on any white space it could be splitted in field9 when there is a white space in field9
//      so here we make one string of the remaining elements and setLineCommand with that string
        for (int i = 8; i < splittedLine.length; i++) {
            fieldNineString += splittedLine[i];
            fieldNineString += " ";

        }
        lf.setLineCommand(fieldNineString);
        return lf;
    }
}
