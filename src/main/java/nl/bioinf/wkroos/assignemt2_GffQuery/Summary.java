/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.wkroos.assignemt2_GffQuery;

import java.util.HashMap;

/**
 *
 * @author Willem
 */
public class Summary {

    private String fileName;

    private final HashMap<String, Integer> moleculesWithFeatures = new HashMap<>();
    private final HashMap<String, Integer> featureHashMap = new HashMap<>();

    /**
     *
     * @param lineFeature this function counts the features
     */
    public void countFeature(LineFeatures lineFeature) {
        if (featureHashMap.containsKey(lineFeature.getFeature())) {

            featureHashMap.put(lineFeature.getFeature(), (featureHashMap.get(lineFeature.getFeature()) + 1));
        } else {

            featureHashMap.put(lineFeature.getFeature(), 1);

        }
    }

    /**
     *
     * @param lineFeature this function counts the seqname
     */
    public void countSeqName(LineFeatures lineFeature) {
        if (moleculesWithFeatures.containsKey(lineFeature.getSeqname())) {

            moleculesWithFeatures.put(lineFeature.getSeqname(), (moleculesWithFeatures.get(lineFeature.getSeqname()) + 1));
        } else {

            moleculesWithFeatures.put(lineFeature.getSeqname(), 1);

        }
    }

    /**
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     *
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "Summary{" + "fileName=" + fileName + ", moleculesWithFeatures=" + moleculesWithFeatures + ", featureHashMap=" + featureHashMap + '}';
    }
}
