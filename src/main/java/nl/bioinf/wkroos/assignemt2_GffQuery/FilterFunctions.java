/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.wkroos.assignemt2_GffQuery;

/**
 *
 * @author Willem
 */
public class FilterFunctions {

    /**
     *
     * @param splittedString
     * @param linefeature splits the filter string and executes the filters when
     * given.
     * @return
     */
    public boolean filterFilterInput(String[] splittedString, LineFeatures linefeature) {

        if (!splittedString[0].equals("*")) {
            if (filterSource(splittedString[0], linefeature) == false) {
                return false;
            }
        }

        if (!splittedString[1].equals("*")) {
            if (filterScore(splittedString[1], linefeature) == false) {
                return false;
            }
        }
        if (!splittedString[2].equals("*")) {
            if (filterOrientation(splittedString[2].charAt(0), linefeature) == false) {
                return false;
            }
        }
        if (!splittedString[3].equals("*")) {
            if (filterMinimumLength(Integer.parseInt(splittedString[3]), linefeature) == false) {
                return false;
            }
        }
        if (!splittedString[4].equals("*")) {
            if (filterMaximumLength(Integer.parseInt(splittedString[4]), linefeature) == false) {
                return false;
            }

        }
        return true;

    }

    /**
     *
     * @param featureType
     * @param lineFeatures checks if the given type equals the type of the line
     * @return
     */
    public boolean getFeature(String featureType, LineFeatures lineFeatures) {
        return featureType.equals(lineFeatures.getFeature());
    }

    /**
     *
     * @param fetchRegion
     * @param lineFeatures checks if the feature lies between the given region
     * @return
     */
    public boolean filterRegion(String fetchRegion, LineFeatures lineFeatures) {
        if (!fetchRegion.isEmpty()) {
            String[] SplittedFetchRegion = fetchRegion.split("\\.\\.");
            return Integer.parseInt(SplittedFetchRegion[0]) < lineFeatures.getStartPosition() && Integer.parseInt(SplittedFetchRegion[1]) > lineFeatures.getEindPosition();
        }

        return false;
    }

    /**
     *
     * @param sourceType
     * @param lineFeatures checks if the given source equals the source of the
     * line
     * @return
     */
    public boolean filterSource(String sourceType, LineFeatures lineFeatures) {
        return sourceType.equals(lineFeatures.getSource());
    }

    /**
     *
     * @param score
     * @param lineFeatures checks if the score equals the score of the line
     * @return
     */
    public boolean filterScore(String score, LineFeatures lineFeatures) {
        return score.equals(lineFeatures.getScore());
    }

    /**
     *
     * @param orientation
     * @param lineFeatures checks if the given orientation is the same as the
     * orientation of the line
     * @return
     */
    public boolean filterOrientation(char orientation, LineFeatures lineFeatures) {
        return orientation == lineFeatures.getStrand();

    }

    /**
     *
     * @param minimumLength
     * @param lineFeatures checks if the line is bigger than given length
     * @return
     */
    public boolean filterMinimumLength(int minimumLength, LineFeatures lineFeatures) {
        return minimumLength < (lineFeatures.getEindPosition() - lineFeatures.getStartPosition());
    }

    /**
     *
     * @param maximumLength
     * @param lineFeatures checks if the line is smaller than given length
     * @return
     */
    public boolean filterMaximumLength(int maximumLength, LineFeatures lineFeatures) {
        return maximumLength > (lineFeatures.getEindPosition() - lineFeatures.getStartPosition());
    }

}
